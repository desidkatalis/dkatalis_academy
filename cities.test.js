let city = require('./cities')

const cities = ['Jakarta', 'Bogor', 'Surabaya', 'Tangerang', 'Bekasi', 'Depok']

test('cities map', () => {
    expect(city.map(cities)).toEqual([7, 5, 8, 9, 6, 5])
})

test('cities filter', () => {
    expect(city.filter(cities)).toEqual([7, 8, 9])
})

test('cities length', () => {
    expect(city.length(cities)).toBe(3)
})

test('cities sort', () => {
    expect(city.sort(cities)).toEqual(['Tangerang', 'Surabaya', 'Jakarta', 'Bekasi', 'Bogor', 'Depok'])
})